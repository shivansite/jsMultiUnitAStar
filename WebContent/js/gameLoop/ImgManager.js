/**
 * @Constructor
 */
function ImgManager() {
	
	/**
	 * @public
	 * @param(Array) imgLocations - Array of Strings representing the locations of the images to load
	 * @returns an Array of Image objects representing the loaded images
	 */
	this.loadImages = function(imgLocations) {
		var imgs = new Array();
		for(var i=0; i<imgLocations.length; i++) {
			var image = new Image();
			image.src=imgLocations[i];
			imgs[i] = image;
		}
		return imgs;
	};
};
