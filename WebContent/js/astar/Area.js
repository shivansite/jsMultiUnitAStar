/**
 * @constructor
 * @param {number} width
 * @param {number} height
 * @param {Array} somewhatPassablePoints - A multidimensional array (of arrays of 2 elements) containing x, y coordinates of each point
 * @param {Array} impassablePoints - A multidimensional array (of arrays of 2 elements) containing x, y coordinates of each point
 * @returns {Area}
 */
function Area(width, height, somewhatPassablePoints, impassablePoints) {
	
	/**
	 * @private
	 */
	this.width = width;
	
	/**
	 * @private
	 */
	this.height = height;
	
	/**
	 * @private
	 */
	this.somewhatPassablePoints = somewhatPassablePoints;
	
	/**
	 * @private
	 */
	this.impassablePoints = impassablePoints;
	
	/**
	 * @private
	 */
	this.terrain = new Array();	
	
	for(var x=0; x < this.width; x++) {
		this.terrain[x] = new Array();
	}
	
	for(var y=0; y < this.height; y++) {
		for(var x=0; x < this.width; x++) {
			var set = false;
			if(this.impassablePoints!=null) {
				for(var i = 0; i < this.impassablePoints.length; i++) {
					var point = this.impassablePoints[i];
					if(point[0]==x && point[1]==y) {
				    	this.terrain[x][y]=TerrainType.IMPASSABLE;
						set=true;
					}
				}
			}
			
			if(this.somewhatPassablePoints!=null) {
				for(var i = 0; i < this.somewhatPassablePoints.length; i++) {
					var point = this.somewhatPassablePoints[i];
					if(point[0]==x && point[1]==y) {
				    	this.terrain[x][y] = TerrainType.SOMEWHAT_PASSABLE;
						set=true;
					}
				}
			}
			
			
			if(!set) {
				this.terrain[x][y]=TerrainType.PASSABLE;
			}
		
		}
	}
};

Area.prototype= {
		
		/**
		 * @public
		 * @memberOf Area.prototype
		 * @param {number} x
		 * @param {number} y
		 * @param {TerrainType} terrainType
		 */
		edit: function(x, y, terrainType) {
			this.terrain[x][y] = terrainType;
		},
		
		/**
		 * @public
		 * @memberOf Area.prototype
		 * @param {number} x
		 * @param {number} y
		 * @returns {TerrainType}
		 */
		getTerrainType: function(x, y) {
			return this.terrain[x][y];
		},
		
		/**
		 * @public
		 * @memberOf Area.prototype
		 * @returns {number}
		 */
		getWidth: function() {
			return this.width;
		},
		
		/**
		 * @public
		 * @memberOf Area.prototype
		 * @returns {number}
		 */
		getHeight: function() {
			return this.height;
		},
		
		/**
		 * @public
		 * @memberOf Area.prototype
		 * @param {Path} path
		 * @param {string} lineTerminator
		 * @returns {String}
		 */
		toString: function(path, lineTerminator) {
			var s = "";
			
			for(var y=0; y<this.terrain[0].length; y++) {
				for(var x=0; x<this.terrain.length; x++) {
					var c = AreaFileTerrainTypeSymbol.PASSABLE;
					var cset = false;
					
					if(path != null && path.nodes != null) {
						for(var i = 0; i < path.nodes.length; i++) {
							var pathNode = path.nodes[i];
							if(pathNode.x==x && pathNode.y==y) {
						    	c = AreaFileTerrainTypeSymbol.PATH;
						    	cset = true;
						    }
						}
					}								
					if(!cset && this.somewhatPassablePoints!=null) {
						for(var i = 0; i < this.somewhatPassablePoints.length; i++) {
							var point = this.somewhatPassablePoints[i];
						    if(point[0]==x && point[1]==y) {
						    	c = AreaFileTerrainTypeSymbol.SOMEWHAT_PASSABLE;
						    }
						}
					}
					if(!cset && this.impassablePoints!=null) {
						for(var i = 0; i < this.impassablePoints.length; i++) {
							var point = this.impassablePoints[i];
						    if(point[0]==x && point[1]==y) {
						    	c = AreaFileTerrainTypeSymbol.IMPASSABLE;
						    }
						}
					}
					s = s + c;
				}
				s = s + lineTerminator;
			}
			return s;
		}
};

