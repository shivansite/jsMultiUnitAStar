var UNIT_SIZE = 3;

const TerrainType = {
	PASSABLE:1,
	SOMWEWHAT_PASSABLE:2,
	IMPASSABLE:0
};

const AreaFileTerrainTypeSymbol = {
		PASSABLE:"_",
		SOMEWHAT_PASSABLE:"x",
		IMPASSABLE:"X",
		PATH:"1"
};