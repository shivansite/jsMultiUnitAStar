/**
 * @constructor 
 * @param {number} areaWidth
 * @param {number} areaHeight
 * @returns {NodeList}
 */
function NodeList(areaWidth, areaHeight) {
	
	/**
	 * @private
	 */
	this.size = 0;
	
	/**
	 * @private
	 */
	this.nodePositionHash = Array();
	
	for(x=0; x<areaWidth; x++) {
		this.nodePositionHash[x] = new Array();
	}
}

NodeList.prototype = {
		
		/**
		 * @public
		 * @memberOf NodeList.prototype
		 * @param {Node} n
		 */
		add: function(n) {
			if(n!=null && this.nodePositionHash[n.x][n.y]==null) {
				this.size++;
			}
			this.nodePositionHash[n.x][n.y] = n;
		},

		/**
		 * @public
		 * @memberOf NodeList.prototype
		 * @param {Number} x
		 * @param {Number} y
		 */
		remove: function(x, y) {
			if(this.size==0) {
				return;
			}
			var existingN = this.nodePositionHash[x][y];
			if(existingN==null) {
				return;
			}
			this.nodePositionHash[x][y] = null;	
			this.size--;
		},

		/**
		 * Returns the first available node in the list
		 * @memberOf NodeList.prototype
		 * @public
		 * @returns {Node}
		 */
		getFirst: function() {
			if(this.size<0) {
				return null;
			}
			for(x=0; x<this.nodePositionHash.length; x++) {
				for(y=0; y<this.nodePositionHash[x].length; y++) {
					var n = this.nodePositionHash[x][y];
					if(n!=null) {
						return n;
					}
				}
			}
			return null;
		},

		/**
		 * @public
		 * @memberOf NodeList.prototype
		 * @returns {Node}
		 */
		searchByPosition: function(x, y) {
			return this.nodePositionHash[x][y];
		}
};