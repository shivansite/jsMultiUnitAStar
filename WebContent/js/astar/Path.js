
/**
 * @constructor
 * @param {Array} nodes - the nodes that make up this Path, in the proper order
 * @param {boolean} reachesDestination
 * @param {number} nodesExploredCount
 * @param {number} computeTimeMs
 * @param {number} exploredNodes
 * @param {number} euclidianLength
 * @returns {Path}
 */
function Path(nodes, reachesDestination, computeTimeMs, exploredNodes) {
	
	/**
	 * @public
	 */
	this.nodes = nodes;
	
	/**
	 * @public
	 */
	this.reachesDestination = reachesDestination;
	
	/**
	 * @public
	 */
	this.computeTimeMs = computeTimeMs;
		
	/**
	 * @public
	 */
	this.exploredNodes = exploredNodes;
}