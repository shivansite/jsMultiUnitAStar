/**
 * @constructor
 */
function GameEntitiesDebugManager(gameEntities) {
	
	this.gameEntitiesReachedDestinationCount = 0;
	
	this.globalStandStateDurationMs = 0;
	this.globalCalculatePathStateDurationMs = 0;
	this.globalEvaluatePathStateDurationMs = 0;
	this.globalMoveBetweenPathNodesStateDurationMs = 0;
	this.globalWaitForBetterPathStateDurationMs = 0;
	
	
	this.gameEntities = gameEntities;
	
	this.onGameEntityReachedDestionation = function(gameEntity) {
		this.gameEntitiesReachedDestinationCount++;
		if(this.gameEntitiesReachedDestinationCount == this.gameEntities.length) {
			this.gameEntitiesReachedDestinationCount = 0;
			var roundDuration = new Date().getTime() - ROUND_START_TIME;
			DEBUG_CONSOLE.clearConsole();
			DEBUG_CONSOLE.toConsole("===============================================================");
			DEBUG_CONSOLE.toConsole("==== Units reached all their destinations in [[[ "+this.getTwoDecimalsValue(roundDuration)+" ms. ]]] ==============");
			DEBUG_CONSOLE.toConsole("===============================================================");
			DEBUG_CONSOLE.toConsole("Individual unit average times spent in each state (disregarding game loop state update wait times) are:");
			DEBUG_CONSOLE.toConsole("");
			this.onGameEntitiesReachedDestinations();
		}
	};
	
	this.onGameEntitiesReachedDestinations = function() {
		$("#moveButton").show();
		$("#clearObstaclesButton").show();
		$("#distanceCalculatorControl").show();
		for(var i = 0; i<this.gameEntities.length; i++) {
			var gameEntity = this.gameEntities[i];			
			gameEntity.switchPositionSide();
			
			var fsm = gameEntity.finiteStateMachine;
			this.globalStandStateDurationMs += fsm.movingUnitStates[fsm.movingUnitStatesEnum.STAND].globalStateDurationMs;
			this.globalCalculatePathStateDurationMs += fsm.movingUnitStates[fsm.movingUnitStatesEnum.CALCULATE_PATH].globalStateDurationMs;
			this.globalEvaluatePathStateDurationMs += fsm.movingUnitStates[fsm.movingUnitStatesEnum.EVALUATE_PATH_ADVANCEMENT].globalStateDurationMs;
			this.globalMoveBetweenPathNodesStateDurationMs += fsm.movingUnitStates[fsm.movingUnitStatesEnum.MOVE_BETWEEN_PATH_NODES].globalStateDurationMs;
			this.globalWaitForBetterPathStateDurationMs += fsm.movingUnitStates[fsm.movingUnitStatesEnum.WAIT_FOR_BETTER_PATH].globalStateDurationMs;
		}
		
		DEBUG_CONSOLE.toConsole(" - Average unit standing around time: "+(this.getTwoDecimalsValue(this.globalStandStateDurationMs/this.gameEntities.length))+" ms.");
		DEBUG_CONSOLE.toConsole(" - Average unit calculating path time: "+(this.getTwoDecimalsValue(this.globalCalculatePathStateDurationMs/this.gameEntities.length))+" ms.");
		DEBUG_CONSOLE.toConsole(" - Average unit evaluating path time: "+(this.getTwoDecimalsValue(this.globalEvaluatePathStateDurationMs/this.gameEntities.length))+" ms.");
		DEBUG_CONSOLE.toConsole(" - Average unit moving between path nodes time: "+(this.getTwoDecimalsValue(this.globalMoveBetweenPathNodesStateDurationMs/this.gameEntities.length))+" ms.");
		DEBUG_CONSOLE.toConsole(" - Average unit waiting for better path time: "+(this.getTwoDecimalsValue(this.globalWaitForBetterPathStateDurationMs/this.gameEntities.length))+" ms.");
 	
		this.globalStandStateDurationMs = 0;
		this.globalCalculatePathStateDurationMs = 0;
		this.globalEvaluatePathStateDurationMs = 0;
		this.globalMoveBetweenPathNodesStateDurationMs = 0;
		this.globalWaitForBetterPathStateDurationMs = 0;
	};
 	
 	this.getTwoDecimalsValue = function(value) {
 		return new Number(value).toFixed(2);
 	};
}