var TerrainCreationMode = {
			CREATE_IMPASSABLE:1,
			CREATE_PASSABLE:2,
			NONE:0
		};

/**
 * @constructor
 * @param(Area) AStar area
 * 
 */
function AStarAreaGameEntity(area, terrainOnlyArea, width, height, cellSize) {
	
	$.extend(this,new GameEntity(0, 0, width, height));
	this.area = area;
	this.terrainOnlyArea = terrainOnlyArea;
	this.cellSize = cellSize;
	this.cellsX = Math.floor(this.width/this.cellSize);
	this.cellsY = Math.floor(this.height/this.cellSize);
	
	this.terrainCreationMode = TerrainCreationMode.NONE;
	
	this.cells = new Array();
	for(var x=0; x < this.cellsX; x++) {
		this.cells[x] = new Array();
	}
	for(var y=0; y < this.cellsY; y++) {
		for(var x=0; x < this.cellsX; x++) {
			this.cells[x][y] = new AStarAreaGameEntityCell(x,y,this.cellSize,this.area, this.terrainOnlyArea);
		}
	}
	
	/**
	 * @public
	 */
	this.processInput = function(inputEvent) {
		if(inputEvent instanceof MouseEvent) {
			var cell = this.getCellAt(inputEvent.x, inputEvent.y);			
			if(inputEvent.mouseEventType == MouseEventType.MOUSE_MOVE) {
				//mouse over
				this.clearCellsMouseOver();
				cell.isMouseOver = true;
				//terrain editing
				if(this.terrainCreationMode == TerrainCreationMode.CREATE_IMPASSABLE) {
					this.area.edit(cell.cellX, cell.cellY, TerrainType.IMPASSABLE);
					this.terrainOnlyArea.edit(cell.cellX, cell.cellY, TerrainType.IMPASSABLE);
				} else if(this.terrainCreationMode == TerrainCreationMode.CREATE_PASSABLE) {
					this.area.edit(cell.cellX, cell.cellY, TerrainType.PASSABLE);
					this.terrainOnlyArea.edit(cell.cellX, cell.cellY, TerrainType.PASSABLE);
				}
			} else if(inputEvent.mouseEventType == MouseEventType.MOUSE_LEFT_DOWN) {
				this.terrainCreationMode = TerrainCreationMode.CREATE_IMPASSABLE;
				this.area.edit(cell.cellX, cell.cellY, TerrainType.IMPASSABLE);
				this.terrainOnlyArea.edit(cell.cellX, cell.cellY, TerrainType.IMPASSABLE);
			} else if(inputEvent.mouseEventType == MouseEventType.MOUSE_LEFT_UP) {
				this.terrainCreationMode = TerrainCreationMode.NONE;
			} else if(inputEvent.mouseEventType == MouseEventType.MOUSE_RIGHT_DOWN) {
				this.terrainCreationMode = TerrainCreationMode.CREATE_PASSABLE;
				this.area.edit(cell.cellX, cell.cellY, TerrainType.PASSABLE);
				this.terrainOnlyArea.edit(cell.cellX, cell.cellY, TerrainType.PASSABLE);
			} else if(inputEvent.mouseEventType == MouseEventType.MOUSE_RIGHT_UP) {
				this.terrainCreationMode = TerrainCreationMode.NONE;
			}
		}
	};
	
	/**
	 * @public
	 */
	this.updateState = function() {
		
	};
	
	/**
	 * @public
	 * @param{Context} canvas
	 */
	this.updateGraphics = function(context) {
		for(y=0; y < this.cellsY; y++) {
			for(x=0; x < this.cellsX; x++) {
				this.cells[x][y].drawTerrain(context);
			}
		}
		for(y=0; y < this.cellsY; y++) {
			for(x=0; x < this.cellsX; x++) {
				this.cells[x][y].drawMouseOver(context);
			}
		}
	};
	
	this.getCellAt = function(canvasX, canvasY) {
		var cellX = Math.floor(canvasX/cellSize);
		var cellY = Math.floor(canvasY/cellSize);
		return this.cells[cellX][cellY];
	};
	
	this.clearCellsMouseOver = function() {
		for(y=0; y < this.cellsY; y++) {
			for(x=0; x < this.cellsX; x++) {
				this.cells[x][y].isMouseOver = false;
			}
		}
	};
}

function AStarAreaGameEntityCell(cellX, cellY, cellSize, aStarArea, terrainOnlyArea) {
	this.cellX = cellX;
	this.cellY = cellY;
	this.cellSize = cellSize;
	this.canvasX = this.cellX * this.cellSize;
	this.canvasY = this.cellY * this.cellSize;
	this.isMouseOver = false;
	this.aStarArea = aStarArea;
	this.terrainOnlyArea = terrainOnlyArea;
	
	this.drawTerrain = function(context) {
		//terrain
		if(DEBUG_MODE) {
			if(aStarArea.getTerrainType(this.cellX, this.cellY) == TerrainType.IMPASSABLE) {
				context.fillStyle="#6E6E6E";
				context.fillRect(this.canvasX,
						this.canvasY,
						this.cellSize,
						this.cellSize);
			}			
		}  else {
			if(terrainOnlyArea.getTerrainType(this.cellX, this.cellY) == TerrainType.IMPASSABLE) {
				context.drawImage(OBSTACLE_SPRITE,
						0, 0, 64, 64, 
						this.cellX*CANVAS_CELL_SIZE-CANVAS_CELL_SIZE/4, this.cellY*CANVAS_CELL_SIZE-CANVAS_CELL_SIZE/4,
						CANVAS_CELL_SIZE+CANVAS_CELL_SIZE/2, CANVAS_CELL_SIZE+CANVAS_CELL_SIZE/2);
			}
		}
		
		//cell outline
		if(SHOW_GRID) {
			context.strokeStyle="#D3A85D";
			context.lineWidth=0.25;
			context.strokeRect(this.canvasX,
					this.canvasY,
					this.cellSize,
					this.cellSize);
		}		
	};
	
	this.drawMouseOver = function(context) {
		if(this.isMouseOver) {
			context.strokeStyle="#FF8900";
			context.lineWidth=1;
			context.strokeRect(this.canvasX,
					this.canvasY,
					this.cellSize,
					this.cellSize);
		}	
	};
}