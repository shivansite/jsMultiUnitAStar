function main(cellsX, cellsY, cellSize) {
	
	DISTANCE_CALCULATOR = DISTANCE_CALCULATOR_ECULID;
	
	var canvasWidth = cellsX * cellSize;
	var canvasHeight = cellsY * cellSize;
	
	var aStarArea = new Area(cellsX, cellsY, null, null);
	//area stores only player-declared obstacles, NOT those set by units during their movement
	var terrainOnlyArea = new Area(cellsX, cellsY, null, null);
		
	var gameEntitiesLayer0 = new Array();
	gameEntitiesLayer0[0] = new AStarAreaGameEntity(aStarArea, terrainOnlyArea, canvasWidth, canvasHeight, cellSize);
	
	var gameEntitiesLayer1 = new Array();
	for(var y=0; y<cellsY-1; y=y+2) {
		gameEntitiesLayer1[Math.floor(y/2)]= new MovingUnitGameEntity(1, y, cellsX-2, y, 3,cellSize, 				
				ANIMATION_BUNDLE_CREATOR.createHumanAnimationBundle(),				
				aStarArea, terrainOnlyArea);
	}
	
	for(var y=1; y<cellsY-1; y=y+2) {
		gameEntitiesLayer1[Math.floor(((cellsY-2)/2)+(y/2))]= new MovingUnitGameEntity(cellsX-2, y, 1, y, 3,cellSize, 
				ANIMATION_BUNDLE_CREATOR.createSkeletonAnimationBundle(),				
				aStarArea, terrainOnlyArea);
	}
	
	var gameEntitiesDebugManager = new GameEntitiesDebugManager(gameEntitiesLayer1);
	for(var i = 0; i<gameEntitiesLayer1.length; i++) {
		gameEntitiesLayer1[i].manager = gameEntitiesDebugManager;
	}
		
	var inputQueue = new Queue(1000);
	new CanvasMouseListener(inputQueue);
	new ControlsInputManager(inputQueue, aStarArea, terrainOnlyArea);
	
	var perfTracker = new GameLoopPerformanceTracker("measurementsOutput");
	var gameLoop = new GameLoop(gameEntitiesLayer0, gameEntitiesLayer1, inputQueue, perfTracker);
	gameLoop.start(canvasWidth, canvasHeight);
}